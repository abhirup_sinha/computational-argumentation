# Computational Argumentation assignments
A repository containing assignment exercises of "Computational Argumentation" (SS21) course in the Universität Paderborn.

Assignment 1 : Data Acquisition  
Assignment 2 : Argument Mining  
Assignment 3 : Argument Assessment  
Assignment 4 : Argument Generation  
