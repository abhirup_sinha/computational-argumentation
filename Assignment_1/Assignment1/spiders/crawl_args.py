# -*- coding: utf-8 -*-
"""
@author: Lannisters
"""
import requests
import scrapy  
import json
from scrapy.selector import Selector
import time  

class CrawlDebatesSpider(scrapy.Spider):
    
    name = "debate_crawler"
    scrape_flag = True

    def start_requests(self):
        #predefinded page to crawl for most popular debates
        url = 'https://www.debate.org/opinions/?sort=popular'
        request = scrapy.Request(url=url, callback=self.parse_url) 
        yield request
        
    def parse_url(self, response): #Function to parse URLs of top 5 popular topics
        href_tags = response.css('a[class=a-image-contain] ::attr(href)')
        urls = []
        counter = 0
        for href_tag in href_tags: #retrieve the URL slugs       	
            counter += 1
            href = href_tag.get().split('=')
            href_1 = ''.join(href)
            url = "https://www.debate.org" + href_1          
            urls.append(url)
            if counter == 5: #Got the top 5 popular topics
                break
        for url in urls: #crawling the url for arguments
            request_new = scrapy.Request(url=url, callback=self.parse_args)
            yield request_new
            
    def parse_args(self, response): #Function to crawl arguments for each topic URL
        category = response.css('#breadcrumb a ::text').getall()[2].strip() #Debate Category
        topic = response.css('#breadcrumb span ::text').get().strip() #Debate Topic
        did = response.css('#voting').attrib['did'] #Debate ID
        #Request URL
        url = 'https://www.debate.org/opinions/~services/opinions.asmx/GetDebateArgumentPage'
        #Request Header
        header = {'X-Requested-With': 'XMLHttpRequest', 
                  'Content-Type': 'application/json; charset=UTF-8'
                  }
        
        load_more = True #Flag to denote there's arguments to get
        page_no = 0
        yes_args = []
        no_args = []
        
        while(load_more): #Loop runs until last page
            page_no += 1
            #Request Payload
            payload = {"debateId":did,"pageNumber":page_no,"itemsPerPage":10,
                    "ysort":5,"nsort":5}
            time.sleep(0.25) #add delay of 0.25 seconds
            page = requests.post(url, data=json.dumps(payload), headers=header)
            
            #Is it Last Page? - Check Condition
            if "{ddo.split}finished" in page.text: #Last Page
                load_more = False
            elif "{ddo.split}needmore" in page.text: #Not Last Page
                load_more = True
                
            data = json.loads(page.text)
            data = data['d'] #Get the page HTML from JSON response
            
            #get desired <li> tags or {ddo.split} text fragments
            lists = Selector(text=data).css('body').re('<li|{ddo.split}[a-z]*')
            print("Topic:-"+topic)
            print("Page No:-"+str(page_no))
            
            #Find the positions of {ddo.split} text fragments
            splits = [i for i, item in enumerate(lists) if item.startswith("{ddo.split}")]
            
            #Limits to denote starts and ends for pro and con arguments
            pro_start = 0
            pro_end = splits[0]
            neg_start = pro_end
            neg_end = splits[1]
            
            #List of all debate arguments in a page
            debates = Selector(text=data).css('.hasData')
            
            #Get pro and con debates
            try:
                for i in range(pro_start, pro_end): 
                    args_title = debates[i].css('h2 ::text').get().strip()
                    args_body_raw = debates[i].css('p ::text').getall()
                    args_body = ''.join(args_body_raw)
                    yes_args.append({"title": args_title, "body": args_body})
                
                for i in range(neg_start,neg_end):
                    args_title = debates[i].css('h2 ::text').get().strip()
                    args_body_raw = debates[i].css('p ::text').getall()
                    args_body = ''.join(args_body_raw)                                   
                    no_args.append({"title": args_title, "body": args_body})
            except IndexError: #happens when there's only pro arguments
                pass
                
        yield {
            "topic" : topic,
            "category" : category,
            "pro_arguments" : yes_args,
            "con_arguments" : no_args
            }