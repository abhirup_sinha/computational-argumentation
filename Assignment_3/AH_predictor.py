# -*- coding: utf-8 -*-
"""
@author: Lannisters
"""


import json
import pandas as pd
import spacy
from spacytextblob.spacytextblob import SpacyTextBlob
import re
import sys
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.preprocessing import RobustScaler

pd.options.mode.chained_assignment = None 
nlp = spacy.load("en_core_web_sm")
nlp.add_pipe('spacytextblob')

AH_you_phrases = ["shouldn't you", 'come you', 'your have', "aren't you",
                  "didn't you", "you'll", 'you do', "you'll not", 'can you not',
                  'you might not', 'you can not', 'have you never', "you didn't",
                  "you don't", 'you could not', 'would you not', 'you can never',
                  "you can't", 'was you', 'dare you', 'did you', 'you did',
                  'did you not', 'you been', 'should you', 'you must not',
                  "you'd never", "you aren't", "you're", 'will you', 'are you',
                  "'d you", "you haven't", 'you were', 'you wanna', 'you must',
                  'do you not']


def preprocess_data(rawtext):
    newtext=removeurl(rawtext)
    newtext=removechars(newtext)   
    return newtext

def removeurl(rawtext): #Removes URL like structure from text
    text=re.sub("(http|ftp|https):\/\/([\w_-]+(?:(?:\.[\w_-]+)+))([\w.,@?^=%&:\/~+#-]*[\w@?^=%&\/~+#-])?","",rawtext)
    text=re.sub("([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9_-]+)","",text)
    return text

def removechars(rawtext): #Removes numbers, unwanted characters from text
    text=rawtext
    text=text.strip().lower()
    text=re.sub("[\[\]]|[^A-z.!?\']"," ",text)
    text=re.sub("[.]+",".",text)
    text=re.sub("[!]+","!",text)
    text=re.sub("[?]+","?",text)
    text=re.sub("[\s]+"," ",text)
    s=[]
    for i in text.split():
         s.append(i)
    return " ".join(s) 

#Finds occurences of you-phrases in text, that commonly leads to Ad-Hominem attacks 
def find_AH_youphrases(text): 
    txt = text.lower()
    isPresent = False
    counter = 0
    for phrase in AH_you_phrases:
        if phrase in txt:
            counter += 1
    if counter != 0:
        isPresent = True
    return (isPresent, counter)

#calculate sentiment polarity difference between two texts
def diffpolarity(text1,text2):  
    doc1 = nlp(text1)
    a=doc1._.polarity 
    doc2 = nlp(text2)
    b=doc2._.polarity 
    x1=(b-a)
    return x1

def lemmatize(text): #Performs lemmatization
    doc=nlp(text)
    tokenlist=[]
    for i in doc: 
        if (not i.is_space and not i.is_punct and not i.like_num 
            and i.ent_type == 0 and len(i.text)>2):
            if(len(i.lemma_)>2):
                tokenlist.append(i.lemma_.lower()) 
    return " ".join(tokenlist)

def load_data(data):
    
    data_df = pd.DataFrame()
    data_record = []
    for record in data:
        votes = {'upvotes':0,'downvotes':0}
        ID = record['id']
        label = record['label']
        convo1 = record['preceding_posts'][0]['body']
        AHyouphrase_details_1 = find_AH_youphrases(convo1)
        isAHYouPhrase_in_convo1 = AHyouphrase_details_1[0] #Presence of AH you-phrases
        numAHYouPhrase_in_convo1 = AHyouphrase_details_1[1] #Count of AH you-phrases
        author1 = record['preceding_posts'][0]['author_name']
        if(record['preceding_posts'][0]['ups'] > 0):
            votes['upvotes'] += record['preceding_posts'][0]['ups'] 
        elif(record['preceding_posts'][0]['ups'] < 0):
            votes['downvotes'] += record['preceding_posts'][0]['ups']
        convo2 = record['preceding_posts'][1]['body']
        AHyouphrase_details_2 = find_AH_youphrases(convo2) 
        isAHYouPhrase_in_convo2 = AHyouphrase_details_2[0] #Presence of AH you-phrases
        numAHYouPhrase_in_convo2 = AHyouphrase_details_2[1] #Count of AH you-phrases
        author2 = record['preceding_posts'][1]['author_name']
        if(record['preceding_posts'][1]['ups'] > 0):
            votes['upvotes'] += record['preceding_posts'][1]['ups']
        elif(record['preceding_posts'][1]['ups'] < 0):
            votes['downvotes'] += record['preceding_posts'][1]['ups']
        convo1 = preprocess_data(convo1) #removes URL and unwanted characters
        convo2 = preprocess_data(convo2)
        polarityChange = diffpolarity(convo1, convo2) #calculate sentiment difference
        convo1 = lemmatize(convo1) #Perform lemmatization
        convo2 = lemmatize(convo2)
        data_record.append([ID,convo1,convo2,author1,author2,votes['upvotes'],
                             votes['downvotes'],isAHYouPhrase_in_convo1,
                             numAHYouPhrase_in_convo1,isAHYouPhrase_in_convo2,
                             numAHYouPhrase_in_convo2,polarityChange,label])
    data_df = data_df.append(data_record)
    data_df.columns=['id','convo1','convo2','author1','author2','upvotes','downvotes',
                   'is_AH_youphrase_in_convo1','numOfAH_youphrase_in_convo1',
                   'is_AH_youphrase_in_convo2','numOfAH_youphrase_in_convo2',
                   'change_in_sentiment','label']
    return data_df

def classify(train, test):
    
    print("Transforming Dataset into TF-IDF features.....")
    #Initialize a TF-IDF vectorizer to convert the documents into TF-IDF feature matrix
    vectorizer = TfidfVectorizer(max_df=0.8)
    X = vectorizer.fit_transform(train['convo1']+" "+train['convo2']).toarray() 
    X = pd.DataFrame(data = X, columns = vectorizer.get_feature_names())
    
    #Add additional features
    X['upvotes'] = train['upvotes'] 
    X['downvotes'] = train['downvotes'] 
    X['is_AH_youphrase_in_convo1'] = train['is_AH_youphrase_in_convo1']
    X['numOfAH_youphrase_in_convo1'] = train['numOfAH_youphrase_in_convo1'] 
    X['is_AH_youphrase_in_convo2'] = train['is_AH_youphrase_in_convo2']
    X['numOfAH_youphrase_in_convo2'] = train['numOfAH_youphrase_in_convo2']
    X['change_in_sentiment'] = train['change_in_sentiment'] 
    
    Xpred=vectorizer.transform(test['convo1']+" "+test['convo2']).toarray()
    Xpred = pd.DataFrame(data = Xpred, columns = vectorizer.get_feature_names()) 
    Xpred['upvotes'] = test['upvotes'] 
    Xpred['downvotes'] = test['downvotes'] 
    Xpred['is_AH_youphrase_in_convo1'] = test['is_AH_youphrase_in_convo1']
    Xpred['numOfAH_youphrase_in_convo1'] = test['numOfAH_youphrase_in_convo1'] 
    Xpred['is_AH_youphrase_in_convo2'] = test['is_AH_youphrase_in_convo2']
    Xpred['numOfAH_youphrase_in_convo2'] = test['numOfAH_youphrase_in_convo2']
    Xpred['change_in_sentiment'] = test['change_in_sentiment']
    
    print("Performing Feature Scaling......")
    #RobustScaler does not suffer from outliers, used to scale the numeric features
    scaler = RobustScaler()
    X[['upvotes','downvotes','numOfAH_youphrase_in_convo1',
       'numOfAH_youphrase_in_convo2']] = scaler.fit_transform(X[['upvotes',
                                 'downvotes','numOfAH_youphrase_in_convo1',
                                 'numOfAH_youphrase_in_convo2']])
    Xpred[['upvotes','downvotes','numOfAH_youphrase_in_convo1',
           'numOfAH_youphrase_in_convo2']] = scaler.transform(Xpred[['upvotes',
                                     'downvotes','numOfAH_youphrase_in_convo1',
                                     'numOfAH_youphrase_in_convo2']])

    print("Training Classifier......")
    # Logistic Regression
    clf = LogisticRegression(random_state=0,max_iter=2000)
    clf.fit(X.values,train.loc[:,['label']].values.ravel())
    
    print("Predicting results......")
    ypred = clf.predict(Xpred.values)
    test['predicted_label'] = ypred
    
    test = test.drop(['convo1','convo2','author1','author2','upvotes','downvotes',
                   'is_AH_youphrase_in_convo1','numOfAH_youphrase_in_convo1',
                   'is_AH_youphrase_in_convo2','numOfAH_youphrase_in_convo2',
                   'change_in_sentiment','label'], axis=1)
    test = test.set_index('id')
    with open('output.json', 'w', encoding='utf-8') as f:
        json.dump(test.to_dict()['predicted_label'], f, indent=4)
        
    print("Output is written to file output.json")
    
def detectAH(train_file, test_file):
    #------------------ TRAINING SEGMENT ------------------#    
    with open(train_file) as train_json_file:
        train_data = json.load(train_json_file)
    train = load_data(train_data)  
    print("Training Data loaded")
    #------------------ TEST SEGMENT ------------------#
    with open(test_file) as test_json_file:
        test_data = json.load(test_json_file)
    test = load_data(test_data)
    print("Test Data loaded")
    #------------------ CLASSIFICATION ------------------#
    classify(train,test)

if __name__ == "__main__":
    print("Program Starting....")
    train_file = sys.argv[1]
    test_file = sys.argv[2]
    detectAH(train_file, test_file)